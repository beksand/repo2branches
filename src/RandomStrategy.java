import java.util.Random;

/**
 * Created by amen on 8/21/17.
 */
public class RandomStrategy implements IInputStrategy {


    public RandomStrategy() {
    }

    @Override
    public int getString() {
        return 0;
    }

    @Override
    public double getDouble() {
        Random rnd = new Random();
        return rnd.nextDouble();
    }

    @Override
    public int getInt() {
        Random rnd = new Random();
        return rnd.nextInt(1000);
    }
}
