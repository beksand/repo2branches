import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        System.out.println("Witaj!");
        System.out.println("Podaj typ wejścia: (1-random, 2-stdin, 3-plik)");
// AND BEKS
//osoba 1


        // odpowiednik HERO
        InputReader reader = new InputReader();

        Scanner sc = new Scanner(System.in);

        int inputType = sc.nextInt();
        if(inputType == 1){
            reader.setStrategy(new RandomStrategy());
        }else if(inputType == 2){
            reader.setStrategy(new StrategyStdIn());
        }else if(inputType == 3){
            // strategia plik
            // TODO:
        }

        String line = "";
        while(!line.equals("quit")){
            // czytamy z wejscia
            line = sc.nextLine();

            //
            if(line.equals("getstring")){
                System.out.println(reader.requestString());
            }else if(line.equals("getint")){
                System.out.println(reader.requestInt());
            }else if(line.equals("getdouble")){
                System.out.println(reader.requestDouble());
            }else if(line.startsWith("setstrategy")){
                line = line.replace("setstrategy ", "");
                if(line.equals("random")) {
                    reader.setStrategy(new RandomStrategy());
                }else if(line.equals("stdin")){
                    reader.setStrategy(new StrategyStdIn());
                }else if(line.equals("file")){
                    // strategia plik
                    // TODO:
                }
            }
        }

    }
}
