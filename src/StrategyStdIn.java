import java.util.Scanner;

/**
 * Created by amen on 8/21/17.
 */
public class StrategyStdIn implements IInputStrategy {

    public StrategyStdIn() {
    }

    @Override
    public int getInt() {
        Scanner scanner = new Scanner(System.in);
        return scanner.nextInt();

    }

    @Override
    public double getDouble() {
        Scanner scanner = new Scanner(System.in);
        return scanner.nextDouble();
    }

    @Override
    public int getString() {
        Scanner scanner = new Scanner(System.in);
        return scanner.nextInt();
    }

}
