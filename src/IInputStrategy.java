public interface IInputStrategy {
    int getInt();
    int getString();
    double getDouble();
}
